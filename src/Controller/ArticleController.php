<?php
namespace App\Controller;

use App\Entity\Article;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ArticleController extends Controller
{

  /**
   * @Route("/",name="article_list")
   *@Method({"GET"})
   */
  public function index()
  {
      // replace this example code with whatever you need
//$articles=['article1','article2'];
$articles=$this->getDoctrine()->getRepository(Article::class)->findAll();
      //return new Response('<html><body> hrllo </body></html>');
        return $this->render('articles/index.html.twig', array('articles'=>$articles));

  }

  /**
   * @Route("/hello")
   */
  public function smart()
  {
      // replace this example code with whatever you need
      return new Response('<html><body> smart </body></html>');
  }

   /**
    * @Route("/article/new",name="neww")
    * Method({"GET","POST"})
    */
    public function new(Request $request)
    {
      $article=new Article();
      $form=$this->createFormBuilder($article)
      ->add('title',TextType::class,array('attr'=>array('class'=>'form-control')))
      ->add('body',TextareaType::class,array('required'=>false,'attr'=>array('class'=>'form-control')))
      ->add('save',SubmitType::class,array('label'=>'Create','attr'=>array('class'=>'btn btn-primary mt3')))
      ->getForm();

      $form->handleRequest($request);

      if($form->isSubmitted() && $form->isValid()){
        $article=$form->getData();
        $entityManager=$this->getDoctrine()->getManager();
        $entityManager->persist($article);
        $entityManager->flush();

          return $this->redirectToRoute('article_list');
      }

      return $this->render('articles/new.html.twig',array('form'=>$form->createView()));
      }

      /**
       * @Route("/article/edit/{id}",name="edittt")
       * Method({"GET","POST"})
       */
       public function edit(Request $request,$id)
       {
         $article=new Article();
         $article=$this->getDoctrine()->getRepository(Article::class)->find($id);

         $form=$this->createFormBuilder($article)
         ->add('title',TextType::class,array('attr'=>array('class'=>'form-control')))
         ->add('body',TextareaType::class,array('required'=>false,'attr'=>array('class'=>'form-control')))
         ->add('save',SubmitType::class,array('label'=>'update','attr'=>array('class'=>'btn btn-primary mt3')))
         ->getForm();

         $form->handleRequest($request);

         if($form->isSubmitted() && $form->isValid()){

           $entityManager=$this->getDoctrine()->getManager();

           $entityManager->flush();

             return $this->redirectToRoute('article_list');
         }

         return $this->render('articles/edit.html.twig',array('form'=>$form->createView()));
         }



  /**
   * @Route("/article/save")
   */
  public function save()
  {
    $entityManager=$this->getDoctrine()->getManager();
    $article=new Article();
    $article->setTitle('Article two');
    $article->setBody('Article two body sdf sdffs asdd');
    $entityManager->persist($article);
    $entityManager->flush();
    return new Response('saves articel with id' .$article->getId());

  }

  /**
   * @Route("/article/{id}")
   */
   public function show($id)
   {
     $article=$this->getDoctrine()->getRepository(Article::class)->find($id);
           //return new Response('<html><body> hrllo </body></html>');
             return $this->render('articles/show.html.twig', array('article'=>$article));
   }

   /**
    * @Route("/article/delete/{id}")
    * @Method({"DELETE"})
    */
    public function delete(Request $request, $id)
    {

      $article=$this->getDoctrine()->getRepository(Article::class)->find($id);
      $entityManager=$this->getDoctrine()->getManager();

        $entityManager->remove($article);
        $entityManager->flush();

         $response=new Response();
         $response->send();
        //  return new Response('saves articel with id');
          }

}
